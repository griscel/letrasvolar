package com.griscel.a_z;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class TruenoActivity extends Activity {
    TextView tv;
    double resultado=0;
    EditText p1,p2,p3,p4,p5,p6,p7,p8,p9,p10;
    ImageView image,imgv;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trueno_activity);
        p1=findViewById(R.id.parte1);
        p2=findViewById(R.id.parte2);
        p3=findViewById(R.id.parte3);
        p4=findViewById(R.id.parte4);
        p5=findViewById(R.id.parte5);
        p6=findViewById(R.id.parte6);
        p7=findViewById(R.id.parte7);
        p8=findViewById(R.id.parte8);
        p9=findViewById(R.id.parte9);
        p10=findViewById(R.id.parte10);
    }

    public void parrafo(View v){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        String contenido="",fragmento;

        fragmento=v.getContentDescription().toString();
        if (fragmento.equals("fragmento1")){
            contenido=getString(R.string.fragmento1);
        }
        else if (fragmento.equals("fragmento2")){
            contenido=getString(R.string.fragmento2);
        }
        else if (fragmento.equals("fragmento3")){
            contenido=getString(R.string.fragmento3);
        }
        else if (fragmento.equals("fragmento4")){
            contenido=getString(R.string.fragmento4);
        }
        else if (fragmento.equals("fragmento5")){
            contenido=getString(R.string.fragmento5);
        }
        else if (fragmento.equals("fragmento6")){
            contenido=getString(R.string.fragmento6);
        }
        else if (fragmento.equals("fragmento7")){
            contenido=getString(R.string.fragmento7);
        }
        else if (fragmento.equals("fragmento8")){
            contenido=getString(R.string.fragmento8);
        }
        else if (fragmento.equals("fragmento9")){
            contenido=getString(R.string.fragmento9);
        }
        else if (fragmento.equals("fragmento10")){
            contenido=getString(R.string.fragmento10);
        }
        dialogBuilder.setTitle("");
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.resultado_view, null);
        tv=dialogView.findViewById(R.id.restv);
        tv.setText(contenido);
        dialogBuilder.setView(dialogView).setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    public void confirmDialog(View v) {
        if(p1.getText().toString().equals("1")){
            resultado++;
        }
        if(p2.getText().toString().equals("6")){
            resultado++;
        }
        if(p3.getText().toString().equals("3")){
            resultado++;
        }
        if(p4.getText().toString().equals("10")){
            resultado++;
        }
        if(p5.getText().toString().equals("4")){
            resultado++;
        }
        if(p6.getText().toString().equals("5")){
            resultado++;
        }
        if(p7.getText().toString().equals("2")){
            resultado++;
        }
        if(p8.getText().toString().equals("7")){
            resultado++;
        }
        if(p9.getText().toString().equals("8")){
            resultado++;
        }
        if(p10.getText().toString().equals("9")){
            resultado++;
        }
        double porcentaje=0;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Resultado");
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.resultado_view, null);
        porcentaje=resultado*10;
        if(resultado==0){
            tv=dialogView.findViewById(R.id.restv);
            tv.setText("Tu total es de: 0% =(");
        }
        else if(resultado>=1){
            imgv=dialogView.findViewById(R.id.estrella1);
            imgv.setVisibility(View.VISIBLE);
            imgv.setImageDrawable(getDrawable(R.drawable.estrella));
            tv=dialogView.findViewById(R.id.restv);
            tv.setText("¡Casi! Tu total es de: "+porcentaje+"%");
            if(porcentaje>=6){
                imgv=dialogView.findViewById(R.id.estrella2);
                imgv.setVisibility(View.VISIBLE);
                imgv.setImageDrawable(getDrawable(R.drawable.estrella));
                tv=dialogView.findViewById(R.id.restv);
                tv.setText("¡Cerca! Tu total es de: "+porcentaje+"%");
                if(resultado==10){
                    imgv=dialogView.findViewById(R.id.estrella3);
                    imgv.setVisibility(View.VISIBLE);
                    imgv.setImageDrawable(getDrawable(R.drawable.estrella));
                    tv=dialogView.findViewById(R.id.restv);
                    tv.setText("¡Bien! Tu total es de: 100%");
                }
            }
        }
        dialogBuilder.setView(dialogView).setPositiveButton("Volver a intentar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        }).setNegativeButton("Salir", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                finish();
            }
        });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

}
