package com.griscel.a_z;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.graphics.TypefaceCompatApi26Impl;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MemoriaActivity extends AppCompatActivity {
    ImageView image,imgv;
    TextView tv;
    EditText et1,et2,et3,et4,et5,et6;
    double resultado=0;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.memoria_activity);
        et1=findViewById(R.id.imagenET1);
        et2=findViewById(R.id.imagenET2);
        et3=findViewById(R.id.imagenET3);
        et4=findViewById(R.id.imagenET4);
        et5=findViewById(R.id.imagenET5);
        et6=findViewById(R.id.imagenET6);
    }

    public void confirmDialog(View v) {
        if(et1.getText().toString().equals("2")){
            resultado++;
        }
        if(et2.getText().toString().equals("4")){
            resultado++;
        }
        if(et3.getText().toString().equals("5")){
            resultado++;
        }
        if(et4.getText().toString().equals("6")){
            resultado++;
        }
        if(et5.getText().toString().equals("1")){
            resultado++;
        }
        if(et6.getText().toString().equals("3")){
            resultado++;
        }
        Toast.makeText(getApplicationContext(),"x "+et1.getText(),Toast.LENGTH_SHORT).show();
        double porcentaje=0;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Resultado");
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.resultado_view, null);
        porcentaje=resultado*16.67;
        if(resultado==0){
            tv=dialogView.findViewById(R.id.restv);
            tv.setText("Tu total es de: 0% =(");
        }
        else if(resultado>=1){
            imgv=dialogView.findViewById(R.id.estrella1);
            imgv.setVisibility(View.VISIBLE);
            imgv.setImageDrawable(getDrawable(R.drawable.estrella));
            tv=dialogView.findViewById(R.id.restv);
            tv.setText("¡Casi! Tu total es de: "+porcentaje+"%");
            if(porcentaje>=3){
                imgv=dialogView.findViewById(R.id.estrella2);
                imgv.setVisibility(View.VISIBLE);
                imgv.setImageDrawable(getDrawable(R.drawable.estrella));
                tv=dialogView.findViewById(R.id.restv);
                tv.setText("¡Cerca! Tu total es de: "+porcentaje+"%");
                if(resultado==6){
                    imgv=dialogView.findViewById(R.id.estrella3);
                    imgv.setVisibility(View.VISIBLE);
                    imgv.setImageDrawable(getDrawable(R.drawable.estrella));
                    tv=dialogView.findViewById(R.id.restv);
                    tv.setText("¡Bien! Tu total es de: 100%");
                }
            }
        }
        dialogBuilder.setView(dialogView).setPositiveButton("Volver a intentar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        }).setNegativeButton("Salir", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                finish();
            }
        });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

}
