package com.griscel.a_z;

import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class AZAdapter extends ArrayAdapter<String> {

    private Activity activity;
    ArrayList<String> mensajes;
    int r,g,b;
    Random aleatorio;

	public AZAdapter(Activity activity, ArrayList<String> mensajes) {
        super(activity, R.layout.letra_view);
        this.activity = activity;
        this.mensajes = mensajes;
        aleatorio=new Random(System.currentTimeMillis());
    }

    public class ViewHolder {
        protected TextView nameTextView;
        public CardView cv;
    }

    public int getCount() {
        return mensajes.size();
    }

    public long getItemId(int position) {
        return position;
    }

    public String getLetra(int position) {
        return mensajes.get(position);
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        View view = null;
        LayoutInflater inflator = activity.getLayoutInflater();
        view = inflator.inflate(R.layout.letra_view, null);
        final ViewHolder viewHolder = new ViewHolder();

        viewHolder.nameTextView = (TextView) view.findViewById(R.id.contenido);
        viewHolder.cv = (CardView) view.findViewById(R.id.letra);

        r = aleatorio.nextInt(250);
        g = aleatorio.nextInt(250);
        b = aleatorio.nextInt(250);

        if(mensajes.get(position).equals("x")) {
            viewHolder.cv.setBackgroundColor(Color.rgb(229,26,26));
        }
        else if(mensajes.get(position).equals("+")) {
            viewHolder.cv.setBackgroundColor(Color.rgb(73,199,36));
        }
        else{
            viewHolder.cv.setBackgroundColor(Color.argb(80, r, g, b));
        }

        viewHolder.nameTextView.setText(mensajes.get(position));

        if (position != 1 && position != 2 && position != 5 && position != 6 && position != 7 && position != 8
                && position != 9 && position != 11 && position != 13 && position != 14 && position != 17
                && position != 18 && position != 21 && position != 22 && position != 25) {
            viewHolder.nameTextView.setTextColor(Color.WHITE);
        } else {
            viewHolder.nameTextView.setTextColor(Color.BLACK);
        }

        return view;
    }

}

