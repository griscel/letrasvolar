package com.griscel.a_z;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;

public class ElefanteActivity extends AppCompatActivity {
    ImageButton b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11,b12,b13,b14,b15,b16,b17,b18;
    ImageButton b19,b20,b21,b22,b23,b24,b25,b26,b27,b28,b29,b30,b31,b32,b33,b34,b35,b36;
    Paint paint;
    DisplayMetrics metrics;
    ImageView image,imgv;
    TextView tv;
    Button resbtn;
    int puntos=0;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.elefante_activity);

        resbtn=findViewById(R.id.resultado);

        resbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDialog();
            }
        });

        b1=findViewById(R.id.imageButton);
        b2=findViewById(R.id.imageButton2);
        b3=findViewById(R.id.imageButton3);
        b4=findViewById(R.id.imageButton4);
        b5=findViewById(R.id.imageButton5);
        b6=findViewById(R.id.imageButton6);
        b7=findViewById(R.id.imageButton7);
        b8=findViewById(R.id.imageButton8);
        b9=findViewById(R.id.imageButton9);
        b10=findViewById(R.id.imageButton10);
        b11=findViewById(R.id.imageButton11);
        b12=findViewById(R.id.imageButton12);
        b13=findViewById(R.id.imageButton13);
        b14=findViewById(R.id.imageButton14);
        b15=findViewById(R.id.imageButton15);
        b16=findViewById(R.id.imageButton16);
        b17=findViewById(R.id.imageButton17);
        b18=findViewById(R.id.imageButton18);
        b19=findViewById(R.id.imageButton19);
        b20=findViewById(R.id.imageButton20);
        b21=findViewById(R.id.imageButton21);
        b22=findViewById(R.id.imageButton22);
        b23=findViewById(R.id.imageButton23);
        b24=findViewById(R.id.imageButton24);
        b25=findViewById(R.id.imageButton25);
        b26=findViewById(R.id.imageButton26);
        b27=findViewById(R.id.imageButton27);
        b28=findViewById(R.id.imageButton28);
        b29=findViewById(R.id.imageButton29);
        b30=findViewById(R.id.imageButton30);
        b31=findViewById(R.id.imageButton31);
        b32=findViewById(R.id.imageButton32);
        b33=findViewById(R.id.imageButton33);
        b34=findViewById(R.id.imageButton34);
        b35=findViewById(R.id.imageButton35);
        b36=findViewById(R.id.imageButton36);
/////////////////////////////////////////////////
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b2.setClickable(false);
                b3.setClickable(false);
                b1.setClickable(false);
                b1.setBackgroundColor(Color.GREEN);
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b1.setClickable(false);
                b3.setClickable(false);
                b2.setClickable(false);
                b2.setBackgroundColor(Color.GREEN);
            }
        });
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b2.setClickable(false);
                b1.setClickable(false);
                b3.setClickable(false);
                puntos++;
                b3.setBackgroundColor(Color.GREEN);
            }
        });
/////////////////////////////////////////////////
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b5.setClickable(false);
                b6.setClickable(false);
                b4.setClickable(false);
                b4.setBackgroundColor(Color.GREEN);
                puntos++;
            }
        });
        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b4.setClickable(false);
                b6.setClickable(false);
                b5.setClickable(false);
                b5.setBackgroundColor(Color.GREEN);
            }
        });
        b6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b5.setClickable(false);
                b4.setClickable(false);
                b6.setClickable(false);
                b6.setBackgroundColor(Color.GREEN);
            }
        });
//////////////////////////////////////////////
        b7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b8.setClickable(false);
                b9.setClickable(false);
                b7.setClickable(false);
                b7.setBackgroundColor(Color.GREEN);
            }
        });
        b8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b7.setClickable(false);
                b9.setClickable(false);
                b8.setClickable(false);
                b8.setBackgroundColor(Color.GREEN);
            }
        });
        b9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b7.setClickable(false);
                b8.setClickable(false);
                b9.setClickable(false);
                b9.setBackgroundColor(Color.GREEN);
                puntos++;
            }
        });
/////////////////////////////////////////////////
        b10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b11.setClickable(false);
                b12.setClickable(false);
                b10.setClickable(false);
                b10.setBackgroundColor(Color.GREEN);
            }
        });
        b11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b10.setClickable(false);
                b12.setClickable(false);
                b11.setClickable(false);
                b11.setBackgroundColor(Color.GREEN);
                puntos++;
            }
        });
        b12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b10.setClickable(false);
                b11.setClickable(false);
                b12.setClickable(false);
                b12.setBackgroundColor(Color.GREEN);
            }
        });
//////////////////////////////////////////////////
        b13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b15.setClickable(false);
                b14.setClickable(false);
                b13.setClickable(false);
                b13.setBackgroundColor(Color.GREEN);
                puntos++;
            }
        });
        b14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b13.setClickable(false);
                b15.setClickable(false);
                b14.setClickable(false);
                b14.setBackgroundColor(Color.GREEN);
            }
        });
        b15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b13.setClickable(false);
                b14.setClickable(false);
                b15.setClickable(false);
                b15.setBackgroundColor(Color.GREEN);
            }
        });
/////////////////////////////////////////////////////////
        b16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b18.setClickable(false);
                b17.setClickable(false);
                b16.setClickable(false);
                b16.setBackgroundColor(Color.GREEN);
            }
        });
        b17.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b18.setClickable(false);
                b16.setClickable(false);
                b17.setClickable(false);
                b17.setBackgroundColor(Color.GREEN);
                puntos++;
            }
        });
        b18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b16.setClickable(false);
                b17.setClickable(false);
                b18.setClickable(false);
                b18.setBackgroundColor(Color.GREEN);
            }
        });
        /////////////////////////////////////////////////////////
        b19.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b19.setClickable(false);
                b20.setClickable(false);
                b21.setClickable(false);
                b19.setBackgroundColor(Color.GREEN);
                puntos++;
            }
        });
        b20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b19.setClickable(false);
                b20.setClickable(false);
                b21.setClickable(false);
                b20.setBackgroundColor(Color.GREEN);
            }
        });
        b21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b19.setClickable(false);
                b20.setClickable(false);
                b21.setClickable(false);
                b21.setBackgroundColor(Color.GREEN);
            }
        });
        /////////////////////////////////////////////////////////
        b22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b22.setClickable(false);
                b23.setClickable(false);
                b24.setClickable(false);
                b22.setBackgroundColor(Color.GREEN);
            }
        });
        b23.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b22.setClickable(false);
                b23.setClickable(false);
                b24.setClickable(false);
                b23.setBackgroundColor(Color.GREEN);
            }
        });
        b24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b22.setClickable(false);
                b23.setClickable(false);
                b24.setClickable(false);
                b24.setBackgroundColor(Color.GREEN);
                puntos++;
            }
        });
        /////////////////////////////////////////////////////////
        b25.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b25.setClickable(false);
                b26.setClickable(false);
                b27.setClickable(false);
                b25.setBackgroundColor(Color.GREEN);
            }
        });
        b26.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b25.setClickable(false);
                b26.setClickable(false);
                b27.setClickable(false);
                b26.setBackgroundColor(Color.GREEN);
                puntos++;
            }
        });
        b27.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b25.setClickable(false);
                b26.setClickable(false);
                b27.setClickable(false);
                b27.setBackgroundColor(Color.GREEN);
                puntos++;
            }
        });
        /////////////////////////////////////////////////////////
        b28.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b28.setClickable(false);
                b29.setClickable(false);
                b30.setClickable(false);
                b28.setBackgroundColor(Color.GREEN);
                puntos++;
            }
        });
        b29.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b28.setClickable(false);
                b29.setClickable(false);
                b30.setClickable(false);
                b29.setBackgroundColor(Color.GREEN);
            }
        });
        b30.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b28.setClickable(false);
                b29.setClickable(false);
                b30.setClickable(false);
                b30.setBackgroundColor(Color.GREEN);
            }
        });
        /////////////////////////////////////////////////////////
        b31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b31.setClickable(false);
                b32.setClickable(false);
                b33.setClickable(false);
                b31.setBackgroundColor(Color.GREEN);
            }
        });
        b32.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b31.setClickable(false);
                b32.setClickable(false);
                b33.setClickable(false);
                b32.setBackgroundColor(Color.GREEN);
            }
        });
        b33.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b31.setClickable(false);
                b32.setClickable(false);
                b33.setClickable(false);
                b33.setBackgroundColor(Color.GREEN);
                puntos++;
            }
        });
        /////////////////////////////////////////////////////////
        b34.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b34.setClickable(false);
                b35.setClickable(false);
                b36.setClickable(false);
                b34.setBackgroundColor(Color.GREEN);
                puntos++;
            }
        });
        b35.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b34.setClickable(false);
                b35.setClickable(false);
                b36.setClickable(false);
                b35.setBackgroundColor(Color.GREEN);
            }
        });
        b36.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b34.setClickable(false);
                b35.setClickable(false);
                b36.setClickable(false);
                b36.setBackgroundColor(Color.GREEN);
            }
        });

    }

    private void confirmDialog() {
        double porcentaje=0;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Resultado");
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.resultado_view, null);
        DecimalFormat df = new DecimalFormat("#.00");
        porcentaje=puntos*8.33;
        if(puntos==0){
            tv=dialogView.findViewById(R.id.restv);
            tv.setText("Tu total es de: 0% =(");
        }
        else if(puntos>=1){
            imgv=dialogView.findViewById(R.id.estrella1);
            imgv.setVisibility(View.VISIBLE);
            imgv.setImageDrawable(getDrawable(R.drawable.estrella));
            tv=dialogView.findViewById(R.id.restv);
            tv.setText("¡Casi! Tu total es de: "+df.format(porcentaje)+"%");
            if(puntos>=6){
                imgv=dialogView.findViewById(R.id.estrella2);
                imgv.setVisibility(View.VISIBLE);
                imgv.setImageDrawable(getDrawable(R.drawable.estrella));
                tv=dialogView.findViewById(R.id.restv);
                tv.setText("¡Cerca! Tu total es de: "+df.format(porcentaje)+"%");
                if(puntos==12){
                    imgv=dialogView.findViewById(R.id.estrella3);
                    imgv.setVisibility(View.VISIBLE);
                    imgv.setImageDrawable(getDrawable(R.drawable.estrella));
                    tv=dialogView.findViewById(R.id.restv);
                    tv.setText("¡Bien! Tu total es de: 100%");
                }
            }
        }
        dialogBuilder.setView(dialogView).setPositiveButton("Volver a intentar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        }).setNegativeButton("Salir", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                finish();
            }
        });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }
}
