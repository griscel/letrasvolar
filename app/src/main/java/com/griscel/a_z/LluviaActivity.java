package com.griscel.a_z;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class LluviaActivity extends AppCompatActivity {

    ImageButton b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11,b12,b13,b14,b15,b16,b17,b18;
    Paint paint;
    DisplayMetrics metrics;
    ImageView image,imgv;
    TextView tv;
    Button resbtn;
    int puntos=0;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lluvia_activity);

        resbtn=findViewById(R.id.resultado);

        resbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDialog();
            }
        });

        b1=findViewById(R.id.imageButton);
        b2=findViewById(R.id.imageButton2);
        b3=findViewById(R.id.imageButton3);
        b4=findViewById(R.id.imageButton4);
        b5=findViewById(R.id.imageButton5);
        b6=findViewById(R.id.imageButton6);
        b7=findViewById(R.id.imageButton7);
        b8=findViewById(R.id.imageButton8);
        b9=findViewById(R.id.imageButton9);
        b10=findViewById(R.id.imageButton10);
        b11=findViewById(R.id.imageButton11);
        b12=findViewById(R.id.imageButton12);
        b13=findViewById(R.id.imageButton13);
        b14=findViewById(R.id.imageButton14);
        b15=findViewById(R.id.imageButton15);
        b16=findViewById(R.id.imageButton16);
        b17=findViewById(R.id.imageButton17);
        b18=findViewById(R.id.imageButton18);
/////////////////////////////////////////////////
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b2.setClickable(false);
                b3.setClickable(false);
                b1.setClickable(false);
                b1.setBackgroundColor(Color.GREEN);
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b1.setClickable(false);
                b3.setClickable(false);
                b2.setClickable(false);
                b2.setBackgroundColor(Color.GREEN);
                puntos++;
            }
        });
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b2.setClickable(false);
                b1.setClickable(false);
                b3.setClickable(false);
                b3.setBackgroundColor(Color.GREEN);
            }
        });
/////////////////////////////////////////////////
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b5.setClickable(false);
                b6.setClickable(false);
                b4.setClickable(false);
                b4.setBackgroundColor(Color.GREEN);
            }
        });
        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b4.setClickable(false);
                b6.setClickable(false);
                b5.setClickable(false);
                b5.setBackgroundColor(Color.GREEN);
            }
        });
        b6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b5.setClickable(false);
                b4.setClickable(false);
                b6.setClickable(false);
                b6.setBackgroundColor(Color.GREEN);
                puntos++;
            }
        });
//////////////////////////////////////////////
        b7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b8.setClickable(false);
                b9.setClickable(false);
                b7.setClickable(false);
                b7.setBackgroundColor(Color.GREEN);
                puntos++;
            }
        });
        b8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b7.setClickable(false);
                b9.setClickable(false);
                b8.setClickable(false);
                b8.setBackgroundColor(Color.GREEN);
            }
        });
        b9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b7.setClickable(false);
                b8.setClickable(false);
                b9.setClickable(false);
                b9.setBackgroundColor(Color.GREEN);
            }
        });
/////////////////////////////////////////////////
        b10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b11.setClickable(false);
                b12.setClickable(false);
                b10.setClickable(false);
                b10.setBackgroundColor(Color.GREEN);
            }
        });
        b11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b10.setClickable(false);
                b12.setClickable(false);
                b11.setClickable(false);
                b11.setBackgroundColor(Color.GREEN);
                puntos++;
            }
        });
        b12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b10.setClickable(false);
                b11.setClickable(false);
                b12.setClickable(false);
                b12.setBackgroundColor(Color.GREEN);
            }
        });
//////////////////////////////////////////////////
        b13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b15.setClickable(false);
                b14.setClickable(false);
                b13.setClickable(false);
                b13.setBackgroundColor(Color.GREEN);
                puntos++;
            }
        });
        b14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b13.setClickable(false);
                b15.setClickable(false);
                b14.setClickable(false);
                b14.setBackgroundColor(Color.GREEN);
            }
        });
        b15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b13.setClickable(false);
                b14.setClickable(false);
                b15.setClickable(false);
                b15.setBackgroundColor(Color.GREEN);
            }
        });
/////////////////////////////////////////////////////////
        b16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b18.setClickable(false);
                b17.setClickable(false);
                b16.setClickable(false);
                b16.setBackgroundColor(Color.GREEN);
            }
        });
        b17.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b18.setClickable(false);
                b16.setClickable(false);
                b17.setClickable(false);
                b17.setBackgroundColor(Color.GREEN);
            }
        });
        b18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b16.setClickable(false);
                b17.setClickable(false);
                b18.setClickable(false);
                b18.setBackgroundColor(Color.GREEN);
                puntos++;
            }
        });
    }

    private void confirmDialog() {
        double porcentaje=0;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Resultado");
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.resultado_view, null);
        //porcentaje=puntos*16.66;
        if(puntos==0){
            tv=dialogView.findViewById(R.id.restv);
            tv.setText("Tu total es de: 0% =(");
        }
        else if(puntos>=1){
            imgv=dialogView.findViewById(R.id.estrella1);
            imgv.setVisibility(View.VISIBLE);
            imgv.setImageDrawable(getDrawable(R.drawable.estrella));
            tv=dialogView.findViewById(R.id.restv);
            tv.setText("¡Casi! Tu total es de: "+puntos);
            if(puntos>=4){
                imgv=dialogView.findViewById(R.id.estrella2);
                imgv.setVisibility(View.VISIBLE);
                imgv.setImageDrawable(getDrawable(R.drawable.estrella));
                tv=dialogView.findViewById(R.id.restv);
                tv.setText("¡Cerca! Tu total es de: "+puntos);
                if(puntos==6){
                    imgv=dialogView.findViewById(R.id.estrella3);
                    imgv.setVisibility(View.VISIBLE);
                    imgv.setImageDrawable(getDrawable(R.drawable.estrella));
                    tv=dialogView.findViewById(R.id.restv);
                    tv.setText("¡Bien! Tu total es de: 100%");
                }
            }
        }
        dialogBuilder.setView(dialogView).setPositiveButton("Volver a intentar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        }).setNegativeButton("Salir", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                finish();
            }
        });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }
}
