package com.griscel.a_z;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;

public class MenuActivity extends Activity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);
    }

    public void abrirAZ(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void abrirSL(View v) {
        Intent intent = new Intent(this, SolLunaActivity.class);
        startActivity(intent);
    }

    public void abrirBoda(View v) {
        Intent intent = new Intent(this, BodaActivity.class);
        startActivity(intent);
    }

    public void abrirPuente(View v) {
        Intent intent = new Intent(this, PuenteActivity.class);
        startActivity(intent);
    }

    public void abrirPrincesa(View v) {
        Intent intent = new Intent(this, PrincesaCuestionario.class);
        intent.putExtra("cuestionario", R.array.all_questions);
        startActivity(intent);
    }

    public void abrirDiosa(View v) {
        Intent intent = new Intent(this, PrincesaCuestionario.class);
        intent.putExtra("cuestionario", R.array.cuestionario_luna);
        startActivity(intent);
    }

    public void abrirLlorona(View v) {
        Intent intent = new Intent(this, PrincesaCuestionario.class);
        intent.putExtra("cuestionario", R.array.cuestionario_llorona);
        startActivity(intent);
    }

    public void abrirJaguares(View v) {
        Intent intent = new Intent(this, PrincesaCuestionario.class);
        intent.putExtra("cuestionario", R.array.cuestionario_jaguares);
        startActivity(intent);
    }

    public void abrirDiferencias(View v) {
        Intent intent = new Intent(this, DiferenciasActivity.class);
        startActivity(intent);
    }

    public void abrirMemoria(View v) {
        Intent intent = new Intent(this, MemoriaActivity.class);
        startActivity(intent);
    }

    public void abrirTrueno(View v) {
        Intent intent = new Intent(this, TruenoActivity.class);
        startActivity(intent);
    }

    public void abrirMaiz(View v) {
        Intent intent = new Intent(this, CuestionarioCuatro.class);
        intent.putExtra("cuestionario", R.array.cuestionario_maiz);
        startActivity(intent);
    }

    public void abrirXtabay(View v) {
        Intent intent = new Intent(this, CuestionarioCinco.class);
        intent.putExtra("cuestionario", R.array.cuestionario_xtabay);
        startActivity(intent);
    }

    public void abrirLluvia(View v) {
        Intent intent = new Intent(this, LluviaActivity.class);
        startActivity(intent);
    }

    public void abrirFuego(View v) {
        Intent intent = new Intent(this, FuegoActivity.class);
        startActivity(intent);
    }

    public void abrirAmarina(View v) {
        Intent intent = new Intent(this, CuestionarioCinco.class);
        intent.putExtra("cuestionario", R.array.cuestionario_amarina);
        startActivity(intent);
    }

    public void abrirElefante(View v) {
        Intent intent = new Intent(this, ElefanteActivity.class);
        startActivity(intent);
    }

    public void abrirPaleta(View v) {
        Intent intent = new Intent(this, PaletaActivity.class);
        startActivity(intent);
    }

}
