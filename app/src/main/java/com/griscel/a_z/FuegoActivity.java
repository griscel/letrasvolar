package com.griscel.a_z;

import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class FuegoActivity extends FragmentActivity {
    int puntaje;
    int realPuntaje;
    String identIm, id2, texto = "Incorrecto", nomUsu;
    Toast toast1;
    ImageView image, imgv;
    TextView tv;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        nomUsu = getIntent().getStringExtra("usuario");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fuego);
        findViewById(R.id.myimage1).setOnTouchListener(new FuegoActivity.MyTouchListener());
        findViewById(R.id.myimage2).setOnTouchListener(new FuegoActivity.MyTouchListener());
        findViewById(R.id.myimage3).setOnTouchListener(new FuegoActivity.MyTouchListener());
        findViewById(R.id.myimage4).setOnTouchListener(new FuegoActivity.MyTouchListener());
        findViewById(R.id.conim1).setOnTouchListener(new FuegoActivity.MyTouchListener());
        findViewById(R.id.conim2).setOnTouchListener(new FuegoActivity.MyTouchListener());
        findViewById(R.id.conim3).setOnTouchListener(new FuegoActivity.MyTouchListener());
        findViewById(R.id.conim4).setOnTouchListener(new FuegoActivity.MyTouchListener());
        //findViewById(R.id.unouno).setOnDragListener(new MyDragListener());
        //findViewById(R.id.unodos).setOnDragListener(new MyDragListener());
        //findViewById(R.id.unotres).setOnDragListener(new MyDragListener());
        //findViewById(R.id.unocuatro).setOnDragListener(new MyDragListener());
        findViewById(R.id.con1).setOnDragListener(new FuegoActivity.MyDragListener());
        findViewById(R.id.con2).setOnDragListener(new FuegoActivity.MyDragListener());
        findViewById(R.id.con3).setOnDragListener(new FuegoActivity.MyDragListener());
        findViewById(R.id.con4).setOnDragListener(new FuegoActivity.MyDragListener());
        toast1 = Toast.makeText(getApplicationContext(), texto, Toast.LENGTH_SHORT);
        puntaje = 0;
        realPuntaje = 0;
    }

    private final class MyTouchListener implements View.OnTouchListener {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                ClipData data = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
                        view);
                view.startDrag(data, shadowBuilder, view, 0);
                //view.setVisibility(View.INVISIBLE);
                identIm = view.getTag().toString();
                return true;
            } else {
                return false;
            }
        }
    }

    public void ganar() {
        if (puntaje == 4) {
            confirmDialog();
        }
    }

    class MyDragListener implements View.OnDragListener {

        @Override
        public boolean onDrag(View v, DragEvent event) {
            int action = event.getAction();
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    // do nothing
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    break;
                case DragEvent.ACTION_DROP:
                    // Dropped, reassign View to ViewGroup
                    View view = (View) event.getLocalState();
                    ViewGroup owner = (ViewGroup) view.getParent();
                    owner.removeView(view);
                    LinearLayout container = (LinearLayout) v;
                    id2 = container.getTag().toString();
                    if (id2.toString().equals(identIm)) {
                        realPuntaje++;
                    } else {
                        realPuntaje--;
                    }
                    if (id2.toString().equals("fuego1") && identIm.equals("fuego1")) {
                        puntaje += 1;
                        container.addView(view);
                        ganar();
                    } else if (id2.toString().equals("fuego2") && identIm.equals("fuego2")) {
                        puntaje += 1;
                        container.addView(view);
                        ganar();

                    } else if (id2.toString().equals("fuego3") && identIm.equals("fuego3")) {
                        puntaje += 1;
                        container.addView(view);
                        ganar();

                    } else if (id2.toString().equals("fuego4") && identIm.equals("fuego4")) {
                        puntaje += 1;
                        container.addView(view);
                        ganar();

                    } else {
                        toast1.show();
                        owner.addView(view);
                    }
                    view.setVisibility(View.VISIBLE);
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                default:
                    break;
            }
            return true;
        }
    }

    private void confirmDialog() {
        double porcentaje = 0;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Resultado");
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.resultado_view, null);
        //porcentaje=puntos*16.66;
        if (realPuntaje <= 0) {
            tv = dialogView.findViewById(R.id.restv);
            tv.setText("Tu total es de: 0% =(");
        } else if (realPuntaje >= 1) {
            imgv = dialogView.findViewById(R.id.estrella1);
            imgv.setVisibility(View.VISIBLE);
            imgv.setImageDrawable(getDrawable(R.drawable.estrella));
            tv = dialogView.findViewById(R.id.restv);
            tv.setText("¡Casi! Tu total es de: " + realPuntaje);
            if (realPuntaje >= 2) {
                imgv = dialogView.findViewById(R.id.estrella2);
                imgv.setVisibility(View.VISIBLE);
                imgv.setImageDrawable(getDrawable(R.drawable.estrella));
                tv = dialogView.findViewById(R.id.restv);
                tv.setText("¡Cerca! Tu total es de: " + realPuntaje);
                if (realPuntaje == 4) {
                    imgv = dialogView.findViewById(R.id.estrella3);
                    imgv.setVisibility(View.VISIBLE);
                    imgv.setImageDrawable(getDrawable(R.drawable.estrella));
                    tv = dialogView.findViewById(R.id.restv);
                    tv.setText("¡Bien! Tu total es de: 100%");
                }
            }
        }
        dialogBuilder.setView(dialogView).setPositiveButton("Volver a intentar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        }).setNegativeButton("Salir", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                finish();
            }
        });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }
}
