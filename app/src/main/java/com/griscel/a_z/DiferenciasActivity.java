package com.griscel.a_z;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class DiferenciasActivity extends AppCompatActivity {

    Chronometer simpleChronometer;
    int contador=0;
    long elapsed=0;
    ImageView iv;
    TextView tv;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.diferencias);
        simpleChronometer = (Chronometer) findViewById(R.id.simpleChronometer);
        simpleChronometer.setBase(SystemClock.elapsedRealtime() - elapsed);
        simpleChronometer.start();
    }

    public void diferencia(View v){
        v.setVisibility(View.VISIBLE);
        v.setBackgroundColor(Color.RED);
        v.setClickable(false);
        contador++;
        if(contador==4){
            simpleChronometer.stop();
            elapsed = SystemClock.elapsedRealtime() -  simpleChronometer.getBase();
            confirmDialog();

        }
    }

    private void confirmDialog() {
        //String tiempo;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Resultado");
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.resultado_view, null);
        //tiempo=simpleChronometer.getText().toString();
        elapsed/=1000;
        if(elapsed>60){
            iv=dialogView.findViewById(R.id.estrella1);
            iv.setVisibility(View.VISIBLE);
            iv.setImageDrawable(getDrawable(R.drawable.estrella));
            tv=dialogView.findViewById(R.id.restv);
            tv.setText("¡Casi!");
        }
        else if(elapsed>=50&&elapsed<=59){
            iv=dialogView.findViewById(R.id.estrella1);
            iv.setVisibility(View.VISIBLE);
            iv.setImageDrawable(getDrawable(R.drawable.estrella));
            iv=dialogView.findViewById(R.id.estrella2);
            iv.setVisibility(View.VISIBLE);
            iv.setImageDrawable(getDrawable(R.drawable.estrella));
            tv=dialogView.findViewById(R.id.restv);
            tv.setText("¡Cerca!");
        }
        else {
            iv=dialogView.findViewById(R.id.estrella1);
            iv.setVisibility(View.VISIBLE);
            iv.setImageDrawable(getDrawable(R.drawable.estrella));
            iv=dialogView.findViewById(R.id.estrella2);
            iv.setVisibility(View.VISIBLE);
            iv.setImageDrawable(getDrawable(R.drawable.estrella));
            tv=dialogView.findViewById(R.id.restv);
            tv.setText("¡Cerca!");
            iv=dialogView.findViewById(R.id.estrella3);
            iv.setVisibility(View.VISIBLE);
            iv.setImageDrawable(getDrawable(R.drawable.estrella));
            tv=dialogView.findViewById(R.id.restv);
            tv.setText("¡Bien!");
        }

        dialogBuilder.setView(dialogView).setPositiveButton("Volver a intentar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        }).setNegativeButton("Salir", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                finish();
            }
        });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

}
