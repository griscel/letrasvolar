package com.griscel.a_z;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class PaletaActivity extends AppCompatActivity {

    TextView textView2, textView3, textView5, textView7, textView9, textView11, textView12, textView22;
    TextView textView1, textView4, textView6, textView8, textView10, textView13, textView14, textView15;
    TextView textView16, textView17, textView18, textView19, textView20, textView23, textView24, textView25, textView21;
    boolean palabraProcesso;
    int puntos;
    ImageView image, imgv;
    TextView tv;
    Button botonFin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.paleta);

        textView1 = (TextView) findViewById(R.id.cuadro1);
        textView2 = (TextView) findViewById(R.id.cuadro2);
        textView3 = (TextView) findViewById(R.id.cuadro3);
        textView4 = (TextView) findViewById(R.id.cuadro4);
        textView5 = (TextView) findViewById(R.id.cuadro5);
        textView6 = (TextView) findViewById(R.id.cuadro6);
        textView7 = (TextView) findViewById(R.id.cuadro7);
        textView8 = (TextView) findViewById(R.id.cuadro8);
        textView9 = (TextView) findViewById(R.id.cuadro9);
        textView10 = (TextView) findViewById(R.id.cuadro10);
        textView11 = (TextView) findViewById(R.id.cuadro11);
        textView12 = (TextView) findViewById(R.id.cuadro12);
        textView13 = (TextView) findViewById(R.id.cuadro13);
        textView14 = (TextView) findViewById(R.id.cuadro14);
        textView15 = (TextView) findViewById(R.id.cuadro15);
        textView16 = (TextView) findViewById(R.id.cuadro16);
        textView17 = (TextView) findViewById(R.id.cuadro17);
        textView18 = (TextView) findViewById(R.id.cuadro18);
        textView19 = (TextView) findViewById(R.id.cuadro19);
        textView20 = (TextView) findViewById(R.id.cuadro20);
        textView21 = (TextView) findViewById(R.id.cuadro21);
        textView22 = (TextView) findViewById(R.id.cuadro22);
        textView23 = (TextView) findViewById(R.id.cuadro23);
        textView24 = (TextView) findViewById(R.id.cuadro24);
        textView25 = (TextView) findViewById(R.id.cuadro25);

        puntos = 0;
        palabraProcesso = false;
        botonFin = (Button) findViewById(R.id.buttonFinalizar);
        botonFin.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                confirmDialog();
                return false;
            }
        });

        textView2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (palabraProcesso == false) {
                        textView2.setBackgroundColor(Color.BLUE);
                        textView2.setEnabled(false);
                        textView2.setTextColor(Color.WHITE);
                        palabraProcesso = true;
                    }
                }
                return false;
            }
        });

        textView5.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (textView2.isEnabled() == false) {
                        textView5.setBackgroundColor(Color.BLUE);
                        textView5.setEnabled(false);
                        textView5.setTextColor(Color.WHITE);
                        palabraProcesso = false;
                        puntos++;
                    }
                }
                return false;
            }
        });

        textView3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (palabraProcesso == false) {
                        textView3.setBackgroundColor(Color.BLACK);
                        textView3.setEnabled(false);
                        textView3.setTextColor(Color.WHITE);
                        palabraProcesso = true;
                    }
                }
                return false;
            }
        });

        textView7.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (textView3.isEnabled() == false) {
                        textView7.setBackgroundColor(Color.BLACK);
                        textView7.setEnabled(false);
                        textView7.setTextColor(Color.WHITE);
                        palabraProcesso = false;
                        puntos++;
                    }
                }
                return false;
            }
        });

        textView11.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (palabraProcesso == false) {
                        textView11.setBackgroundColor(Color.WHITE);
                        textView11.setEnabled(false);
                        textView11.setTextColor(Color.CYAN);
                        palabraProcesso = true;
                    }
                }
                return false;
            }
        });

        textView9.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (textView11.isEnabled() == false) {
                        textView9.setBackgroundColor(Color.WHITE);
                        textView9.setEnabled(false);
                        textView9.setTextColor(Color.CYAN);
                        palabraProcesso = false;
                        puntos++;
                    }
                }
                return false;
            }
        });

        textView12.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (palabraProcesso == false) {
                        textView12.setBackgroundColor(Color.RED);
                        textView12.setEnabled(false);
                        textView12.setTextColor(Color.WHITE);
                        palabraProcesso = true;
                    }
                }
                return false;
            }
        });

        textView22.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (textView12.isEnabled() == false) {
                        textView22.setBackgroundColor(Color.RED);
                        textView22.setEnabled(false);
                        textView22.setTextColor(Color.WHITE);
                        palabraProcesso = false;
                        puntos++;
                    }
                }
                return false;
            }
        });

        View.OnTouchListener listenerX = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    Random rnd = new Random();
                    int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
                    v.setBackgroundColor(color);
                    v.setEnabled(true);
                    puntos--;
                }
                return false;
            }
        };

        textView1.setOnTouchListener(listenerX);
        textView4.setOnTouchListener(listenerX);
        textView6.setOnTouchListener(listenerX);
        textView8.setOnTouchListener(listenerX);
        textView10.setOnTouchListener(listenerX);
        textView13.setOnTouchListener(listenerX);
        textView14.setOnTouchListener(listenerX);
        textView15.setOnTouchListener(listenerX);
        textView16.setOnTouchListener(listenerX);
        textView17.setOnTouchListener(listenerX);
        textView18.setOnTouchListener(listenerX);
        textView19.setOnTouchListener(listenerX);
        textView20.setOnTouchListener(listenerX);
        textView21.setOnTouchListener(listenerX);
        textView23.setOnTouchListener(listenerX);
        textView24.setOnTouchListener(listenerX);
        textView25.setOnTouchListener(listenerX);
    }

    private void confirmDialog() {
        double porcentaje = 0;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Resultado");
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.resultado_view, null);
        //porcentaje=puntos*16.66;
        if (puntos <= 0) {
            tv = dialogView.findViewById(R.id.restv);
            tv.setText("Tu total es de: 0% =(");
        } else if (puntos >= 1) {
            imgv = dialogView.findViewById(R.id.estrella1);
            imgv.setVisibility(View.VISIBLE);
            imgv.setImageDrawable(getDrawable(R.drawable.estrella));
            tv = dialogView.findViewById(R.id.restv);
            tv.setText("¡Casi! Tu total es de: " + puntos);
            if (puntos >= 3) {
                imgv = dialogView.findViewById(R.id.estrella2);
                imgv.setVisibility(View.VISIBLE);
                imgv.setImageDrawable(getDrawable(R.drawable.estrella));
                tv = dialogView.findViewById(R.id.restv);
                tv.setText("¡Cerca! Tu total es de: " + puntos);
                if (puntos == 4) {
                    imgv = dialogView.findViewById(R.id.estrella3);
                    imgv.setVisibility(View.VISIBLE);
                    imgv.setImageDrawable(getDrawable(R.drawable.estrella));
                    tv = dialogView.findViewById(R.id.restv);
                    tv.setText("¡Bien! Tu total es de: 100%");
                }
            }
        }
        dialogBuilder.setView(dialogView).setPositiveButton("Volver a intentar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        }).setNegativeButton("Salir", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                finish();
            }
        });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

}
