package com.griscel.a_z;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class PuenteActivity extends Activity {
    boolean selected=false;
    int estatus=1,width,height;
    double resultado=0;
    int [] location1,location2;
    float x1,y1,x2,y2;
    CardView t1,t2,t3,t4,t5,t6,t11,t12,t13,t14,t15,t16,s1;
    Paint paint;
    DisplayMetrics metrics;
    ImageView image,imgv;
    TextView tv;
    Button resbtn;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.puente);
        metrics = new DisplayMetrics();
        inicializarComponentes();
        location1 = new int[2];
        location2 = new int[2];
        resbtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                confirmDialog();
            }
        });
    }

    private void inicializarComponentes() {
        t1=findViewById(R.id.tarjeta1);
        t2=findViewById(R.id.tarjeta2);
        t3=findViewById(R.id.tarjeta3);
        t4=findViewById(R.id.tarjeta4);
        t5=findViewById(R.id.tarjeta5);
        t6=findViewById(R.id.tarjeta6);
        t11=findViewById(R.id.tarjeta11);
        t12=findViewById(R.id.tarjeta12);
        t13=findViewById(R.id.tarjeta13);
        t14=findViewById(R.id.tarjeta14);
        t15=findViewById(R.id.tarjeta15);
        t16=findViewById(R.id.tarjeta16);
        resbtn=findViewById(R.id.resultado);
        t11.setClickable(false);
        t12.setClickable(false);
        t13.setClickable(false);
        t14.setClickable(false);
        t15.setClickable(false);
        t16.setClickable(false);
    }

    public void seleccionar(View v){
        if(estatus==1) {
            if (v.getContentDescription().equals("tarjeta1")) {
                s1 = t1;
                t2.setClickable(false);
                t3.setClickable(false);
                t4.setClickable(false);
                t5.setClickable(false);
                t6.setClickable(false);
                t11.setClickable(true);
                t12.setClickable(true);
                t13.setClickable(true);
                t14.setClickable(true);
                t15.setClickable(true);
                t16.setClickable(true);

                estatus = 2;
            } else if (v.getContentDescription().equals("tarjeta2")) {
                t1.setClickable(false);
                s1 = t2;
                t3.setClickable(false);
                t4.setClickable(false);
                t5.setClickable(false);
                t6.setClickable(false);
                t11.setClickable(true);
                t12.setClickable(true);
                t13.setClickable(true);
                t14.setClickable(true);
                t15.setClickable(true);
                t16.setClickable(true);
                s1.setBackgroundColor(Color.rgb(126, 196, 248));
                estatus = 2;
            } else if (v.getContentDescription().equals("tarjeta3")) {
                t1.setClickable(false);
                t2.setClickable(false);
                s1 = t3;
                t4.setClickable(false);
                t5.setClickable(false);
                t6.setClickable(false);
                t11.setClickable(true);
                t12.setClickable(true);
                t13.setClickable(true);
                t14.setClickable(true);
                t15.setClickable(true);
                t16.setClickable(true);
                s1.setBackgroundColor(Color.rgb(126, 196, 248));
                estatus = 2;
            }else if (v.getContentDescription().equals("tarjeta4")) {
                t1.setClickable(false);
                t2.setClickable(false);
                t3.setClickable(false);
                s1 = t4;
                t5.setClickable(false);
                t6.setClickable(false);
                t11.setClickable(true);
                t12.setClickable(true);
                t13.setClickable(true);
                t14.setClickable(true);
                t15.setClickable(true);
                t16.setClickable(true);
                s1.setBackgroundColor(Color.rgb(126, 196, 248));
                estatus = 2;
            }else if (v.getContentDescription().equals("tarjeta5")) {
                t1.setClickable(false);
                t2.setClickable(false);
                t3.setClickable(false);
                t4.setClickable(false);
                s1 = t5;
                t6.setClickable(false);
                t11.setClickable(true);
                t12.setClickable(true);
                t13.setClickable(true);
                t14.setClickable(true);
                t15.setClickable(true);
                t16.setClickable(true);
                s1.setBackgroundColor(Color.rgb(126, 196, 248));
                estatus = 2;
            }else if (v.getContentDescription().equals("tarjeta6")) {
                t1.setClickable(false);
                t2.setClickable(false);
                t3.setClickable(false);
                t4.setClickable(false);
                t5.setClickable(false);
                s1 = t6;
                t11.setClickable(true);
                t12.setClickable(true);
                t13.setClickable(true);
                t14.setClickable(true);
                t15.setClickable(true);
                t16.setClickable(true);
                estatus = 2;
            }
            s1.setBackgroundColor(Color.rgb(249, 249, 91));
        }
        else if(estatus==2){
            if(v.getContentDescription().equals("tarjeta11")){
                v.setBackgroundColor(Color.rgb(255, 195, 0));
                v.setContentDescription("true");
                t1.setClickable(true);
                t2.setClickable(true);
                t3.setClickable(true);
                t4.setClickable(true);
                t5.setClickable(true);
                t6.setClickable(true);
                t11.setClickable(false);
                t12.setClickable(false);
                t13.setClickable(false);
                t14.setClickable(false);
                t15.setClickable(false);
                t16.setClickable(false);
                s1.setClickable(false);
                image = findViewById(R.id.linea1);
                s1.getLocationOnScreen(location1);
                x1=location1[0];
                y1=location1[1];
                v.getLocationOnScreen(location2);
                x2=location2[0];
                y2=location2[1];
                dibujarLinea();
                estatus=1;
                if(s1.getContentDescription().equals("tarjeta6")){
                    resultado++;
                }
                s1.setContentDescription("true");
            }
            else if(v.getContentDescription().equals("tarjeta12")){
                v.setBackgroundColor(Color.rgb(255, 195, 0));
                v.setContentDescription("true");
                t1.setClickable(true);
                t2.setClickable(true);
                t3.setClickable(true);
                t4.setClickable(true);
                t5.setClickable(true);
                t6.setClickable(true);
                t11.setClickable(false);
                t12.setClickable(false);
                t13.setClickable(false);
                t14.setClickable(false);
                t15.setClickable(false);
                t16.setClickable(false);
                s1.setClickable(false);
                image = findViewById(R.id.linea2);
                s1.getLocationOnScreen(location1);
                x1=location1[0];
                y1=location1[1];
                v.getLocationOnScreen(location2);
                x2=location2[0];
                y2=location2[1];
                dibujarLinea();
                estatus=1;
                if(s1.getContentDescription().equals("tarjeta4")){
                    resultado++;
                }
                s1.setContentDescription("true");
            }
            else if(v.getContentDescription().equals("tarjeta13")){
                v.setBackgroundColor(Color.rgb(255, 195, 0));
                v.setContentDescription("true");
                t1.setClickable(true);
                t2.setClickable(true);
                t3.setClickable(true);
                t4.setClickable(true);
                t5.setClickable(true);
                t6.setClickable(true);
                t11.setClickable(false);
                t12.setClickable(false);
                t13.setClickable(false);
                t14.setClickable(false);
                t15.setClickable(false);
                t16.setClickable(false);
                s1.setClickable(false);
                image = findViewById(R.id.linea3);
                s1.getLocationOnScreen(location1);
                x1=location1[0];
                y1=location1[1];
                v.getLocationOnScreen(location2);
                x2=location2[0];
                y2=location2[1];
                dibujarLinea();
                estatus=1;
                if(s1.getContentDescription().equals("tarjeta1")){
                    resultado++;
                }
                s1.setContentDescription("true");
            }
            else if(v.getContentDescription().equals("tarjeta14")){
                v.setBackgroundColor(Color.rgb(255, 195, 0));
                v.setContentDescription("true");
                t1.setClickable(true);
                t2.setClickable(true);
                t3.setClickable(true);
                t4.setClickable(true);
                t5.setClickable(true);
                t6.setClickable(true);
                t11.setClickable(false);
                t12.setClickable(false);
                t13.setClickable(false);
                t14.setClickable(false);
                t15.setClickable(false);
                t16.setClickable(false);
                s1.setClickable(false);
                image = findViewById(R.id.linea4);
                s1.getLocationOnScreen(location1);
                x1=location1[0];
                y1=location1[1];
                v.getLocationOnScreen(location2);
                x2=location2[0];
                y2=location2[1];
                dibujarLinea();
                estatus=1;
                if(s1.getContentDescription().equals("tarjeta3")){
                    resultado++;
                }
                s1.setContentDescription("true");
            }
            else if(v.getContentDescription().equals("tarjeta15")){
                v.setBackgroundColor(Color.rgb(255, 195, 0));
                v.setContentDescription("true");
                t1.setClickable(true);
                t2.setClickable(true);
                t3.setClickable(true);
                t4.setClickable(true);
                t5.setClickable(true);
                t6.setClickable(true);
                t11.setClickable(false);
                t12.setClickable(false);
                t13.setClickable(false);
                t14.setClickable(false);
                t15.setClickable(false);
                t16.setClickable(false);
                s1.setClickable(false);
                image = findViewById(R.id.linea5);
                s1.getLocationOnScreen(location1);
                x1=location1[0];
                y1=location1[1];
                v.getLocationOnScreen(location2);
                x2=location2[0];
                y2=location2[1];
                dibujarLinea();
                estatus=1;
                if(s1.getContentDescription().equals("tarjeta5")){
                    resultado++;
                }
                s1.setContentDescription("true");
            }
            else if(v.getContentDescription().equals("tarjeta16")){
                v.setBackgroundColor(Color.rgb(255, 195, 0));
                v.setContentDescription("true");
                t1.setClickable(true);
                t2.setClickable(true);
                t3.setClickable(true);
                t4.setClickable(true);
                t5.setClickable(true);
                t6.setClickable(true);
                t11.setClickable(false);
                t12.setClickable(false);
                t13.setClickable(false);
                t14.setClickable(false);
                t15.setClickable(false);
                t16.setClickable(false);
                s1.setClickable(false);
                image = findViewById(R.id.linea6);
                s1.getLocationOnScreen(location1);
                x1=location1[0];
                y1=location1[1];
                v.getLocationOnScreen(location2);
                x2=location2[0];
                y2=location2[1];
                dibujarLinea();
                estatus=1;
                if(s1.getContentDescription().equals("tarjeta2")){
                    resultado++;
                }
                s1.setContentDescription("true");
            }
            else {
                s1.setBackgroundColor(Color.rgb(255, 255, 255));
                t1.setClickable(true);
                t2.setClickable(true);
                t3.setClickable(true);
                t4.setClickable(true);
                t5.setClickable(true);
                t6.setClickable(true);
                t11.setClickable(false);
                t12.setClickable(false);
                t13.setClickable(false);
                t14.setClickable(false);
                t15.setClickable(false);
                t16.setClickable(false);
                estatus=1;
            }
        }
    }

    private void dibujarLinea() {

        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        width = metrics.widthPixels;
        height = metrics.heightPixels;

        image.setZ(5);
        Bitmap bitmap = Bitmap.createBitmap(width,height,Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setColor(Color.GREEN);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(10);
        paint.setAntiAlias(true);

        canvas.drawLine(x1+150,y1+50,x2+150,y2+50,paint);

        image.setImageBitmap(bitmap);
    }

    private void confirmDialog() {
        double porcentaje=0;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Resultado");
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.resultado_view, null);
        porcentaje=resultado*16.66;
        if(resultado==0){
            tv=dialogView.findViewById(R.id.restv);
            tv.setText("Tu total es de: 0% =(");
        }
        else if(resultado>=1){
            imgv=dialogView.findViewById(R.id.estrella1);
            imgv.setVisibility(View.VISIBLE);
            imgv.setImageDrawable(getDrawable(R.drawable.estrella));
            tv=dialogView.findViewById(R.id.restv);
            tv.setText("¡Casi! Tu total es de: "+porcentaje+"%");
            if(resultado>=3){
                imgv=dialogView.findViewById(R.id.estrella2);
                imgv.setVisibility(View.VISIBLE);
                imgv.setImageDrawable(getDrawable(R.drawable.estrella));
                tv=dialogView.findViewById(R.id.restv);
                tv.setText("¡Cerca! Tu total es de: "+porcentaje+"%");
                if(resultado==6){
                    imgv=dialogView.findViewById(R.id.estrella3);
                    imgv.setVisibility(View.VISIBLE);
                    imgv.setImageDrawable(getDrawable(R.drawable.estrella));
                    tv=dialogView.findViewById(R.id.restv);
                    tv.setText("¡Bien! Tu total es de: 100%");
                }
            }
        }
        dialogBuilder.setView(dialogView).setPositiveButton("Volver a intentar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        }).setNegativeButton("Salir", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                finish();
            }
        });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }
}
