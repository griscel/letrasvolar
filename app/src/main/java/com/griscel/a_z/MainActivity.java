package com.griscel.a_z;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    String abc=" BC  FGHIJ L NÑ  QR  UV  Y ";
    String respuesta="ABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
    GridView gridView;
    AZAdapter listAdapter;
    ArrayList<String> mensajes;
    String[] items;
    Button finbtn,resbtn;
    ImageView iv;
    TextView tv;
    int resultado=0;
    boolean terminado=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        items = new String[27];
        finbtn=findViewById(R.id.finBtn);
        resbtn=findViewById(R.id.resBtn);
        cargarLetras();

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView,View view, int position, long rowId) {
                if(position!=1&&position!=2&&position!=5&&position!=6&&position!=7&&position!=8
                        &&position!=9&&position!=11&&position!=13&&position!=14&&position!=17
                        &&position!=18&&position!=21&&position!=22&&position!=25&&!terminado) {
                    createRadioListDialog(position);
                }
            }
        });

        finbtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                comprobar(v);
            }
        });

        resbtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                confirmDialog();
            }
        });
    }

    public void createRadioListDialog(int position) {
        final int posicion=position;
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(MainActivity.this);
        mBuilder.setTitle("¿Qué letra va aquí?");
        mBuilder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mensajes.set(posicion,items[i]);
                listAdapter.notifyDataSetChanged();
                dialogInterface.dismiss();
            }
        });

        AlertDialog mDialog = mBuilder.create();
        mDialog.show();
    }

    private void confirmDialog() {
        double porcentaje=0;
        DecimalFormat formato = new DecimalFormat("#.00");
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Resultado");
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.resultado_view, null);
        porcentaje=resultado*8.33;
        if(resultado==0){
            tv=dialogView.findViewById(R.id.restv);
            tv.setText("Tu total es de: 0% =(");
        }
        else if(resultado>0){
            iv=dialogView.findViewById(R.id.estrella1);
            iv.setVisibility(View.VISIBLE);
            iv.setImageDrawable(getDrawable(R.drawable.estrella));
            tv=dialogView.findViewById(R.id.restv);
            tv.setText("¡Casi! Tu total es de: "+formato.format(porcentaje)+"%");
            if(resultado>6){
                iv=dialogView.findViewById(R.id.estrella2);
                iv.setVisibility(View.VISIBLE);
                iv.setImageDrawable(getDrawable(R.drawable.estrella));
                tv=dialogView.findViewById(R.id.restv);
                tv.setText("¡Cerca! Tu total es de: "+formato.format(porcentaje)+"%");
                if(resultado==12){
                    iv=dialogView.findViewById(R.id.estrella3);
                    iv.setVisibility(View.VISIBLE);
                    iv.setImageDrawable(getDrawable(R.drawable.estrella));
                    tv=dialogView.findViewById(R.id.restv);
                    tv.setText("¡Bien! Tu total es de: 100%");
                }
            }
        }
        dialogBuilder.setView(dialogView).setPositiveButton("Volver a intentar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        }).setNegativeButton("Salir", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                finish();
            }
        });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    public void comprobar(View v){
        resultado=0;
        for(int position=0;position<27;position++){
            if (position != 1 && position != 2 && position != 5 && position != 6 && position != 7 && position != 8
                    && position != 9 && position != 11 && position != 13 && position != 14 && position != 17
                    && position != 18 && position != 21 && position != 22 && position != 25) {
                if(mensajes.get(position).equals(String.valueOf(respuesta.charAt(position)))){
                    resultado++;
                    mensajes.set(position,"+");
                    listAdapter.notifyDataSetChanged();
                }
                else {
                    mensajes.set(position,"x");
                    listAdapter.notifyDataSetChanged();;
                }
            }
        }
        terminado=true;
        v.setVisibility(View.GONE);
        resbtn.setVisibility(View.VISIBLE);
    }

    private void cargarLetras() {
        gridView = (GridView) findViewById(R.id.mainGrid);

        mensajes = new ArrayList<String>();
        for (int i=0;i<27;i++) {
            mensajes.add(String.valueOf(abc.charAt(i)));
            items[i]=String.valueOf(respuesta.charAt(i));
        }
        listAdapter = new AZAdapter(this, mensajes);

        gridView.setAdapter(listAdapter);
        gridView.setNumColumns(6);
    }
}
